-- Implementación del juego TicTacToe con el algoritmo Minimax

local Minimax = require 'LuaSearx.Adversarial_search.minimax'

local tictactoe_handler = require 'LuaSearx.handlers.tictactoe_handler'
local minimax = Minimax(handler)

-- Declaramos la tabla que almacena el estado del juego y la variable que indica si el juego ha finalizado
local L_chips = {0,0,0,0,0,0,0,0,0}
local end_game = false

-- La función putChips se encarga de poner ficha en la posición pos (turno del jugador humano).
-- Después, comprueba si el juego ha finalizado y, si no es así, ejecuta el turno de la IA, que
-- utilizará Minimax para elegir su movimiento
function putChips (pos)
    L_chips[pos]= 1
    representChips(L_chips)
    if tictactoe_handler.getResult({L_chips,1},1) == 1 then
        print("Has ganado.")
        end_game = true
    end
    if tictactoe_handler.getResult({L_chips,1},1) == 0.5 then
        print("Empate.")
        end_game = true
    elseif end_game==false then
        local m = minimax:findChoice({copyTable(L_chips),1},true,tictactoe_handler,0)[1]
        print("Yo pongo ficha en " .. m)
        L_chips[m]= 2
        representChips(L_chips)
        if tictactoe_handler.getResult({L_chips,2},2) == 1 then
            print("He ganado.")
            end_game = true
        end
        if tictactoe_handler.getResult({L_chips,2},2) == 0.5 then
            print("Empate.")
            end_game = true
        end
    end
end

-- La función representChips se encarga de representar el tablero del juego de estado t
function representChips (t)
    cas={}
    for i,pos in pairs(t) do
        if     pos == 0 then cas[i]=" "
        elseif pos == 1 then cas[i]="X"
        else                 cas[i]="O"
        end
    end
	print("\n")
	print("    "..cas[1].." | "..cas[2].." | "..cas[3].." ")
	print("    ---------")
	print("    "..cas[4].." | "..cas[5].." | "..cas[6].." ")
	print("    ---------")
	print("    "..cas[7].." | "..cas[8].." | "..cas[9].." ")
	print("\n")
end

representChips(L_chips)

-- La función copyTable se encarga de copiar una tabla. Se usará para evitar problemas derivados de la
-- propiedad de mutabilidad de las tablas
function copyTable(origin)
    if origin == nil then
        return {}
    end
    tabla = {}
    for i,e in ipairs(origin) do
        table.insert(tabla,e)
    end
    return tabla
end

-- Mientras la partida no haya finalizado el programa preguntará al jugador humano
-- en qué posición desea colocar ficha en su turno y para colocarla llamará a la función putChips
while #tictactoe_handler.getRules({L_chips,1}) ~= 0 and end_game==false do
    io.write("Donde quieres poner ficha?\n")
    local pos = io.read("n")
    if pos <= 9 and pos >= 0 and L_chips[pos]==0 then
        io.write("Tu pones ficha en "..pos.."\n")
        putChips(pos)
    else 
        io.write("ERROR\n")
    end
end