-- Implementación del juego Nim con el algoritmo MCTS-UCT

local MCTS = require 'LuaSearx.Adversarial_search.MCTS'

local nim_handler = require 'LuaSearx.handlers.nim_handler'
local mcts = MCTS(nim_handler)

-- Declaramos las variables que almacenan el número de fichas iniciales y las iteraciones que realizará MCTS
local num_chips = 21
local Max_iterations = 10000

-- La función removeChips se encarga de eliminar n fichas del montón (turno del jugador humano).
-- Después, comprueba si el juego ha finalizado y, si no es así, ejecuta el turno de la IA, que
-- utilizará MCTS-UCT para elegir su movimiento
function removeChips (n)
    if n <= num_chips then
        num_chips = num_chips - n
    end
    if num_chips == 0 then
        print("Has ganado.")
    else
        local m = mcts:UCT({num_chips,1},Max_iterations)
        print("Yo elimino " .. m .. " fichas")
        num_chips = num_chips - m
        if num_chips == 0 then
            print("He ganado.")
        end
    end
end

--Mientras la partida no haya finalizado el programa preguntará al jugador humano
-- cuántas fichas desea retirar en su turno y para retirarlas llamará a la función removeChips
while num_chips ~= 0 do
    io.write("Quedan "..num_chips.." fichas\n")
    io.write("Cuantas quieres quitar?\n")
    local num = io.read("n")
    if num < 4 and num > 0 and num <= num_chips then
        io.write("Tu eliminas "..num.." fichas\n")
        removeChips(num)
    else 
        io.write("ERROR\n")
    end
end