﻿# LuaSearx

Search algorithms library for Lua developed by Pablo Sánchez as a final project at the University of Sevilla

The library implements algorithms of following search types:

 - Uninformed search
 - Informed search
 - Adversarial search

Also are included some problems and games that allows to test the different searches. Their implementation is located in the **handlers** folder. The files that builds the search space structure are in the **utils** folder.

## Dependencies

- Lua (version 5.1 or later)
 
## Structure

![enter image description here](https://i.gyazo.com/166d9a95322bf536f32166d8403c5635.png)

## Test files

You can change the problem or game and the algorithm to test it editing the comments zone in each test file, removing `--` at the line start where "INICIO" appears of the current problem and adding `--` at the line start of the next problem

## Run tests

To run a test file having Lua 5.4 installed you can do:

`> lua54 minimax_nim.lua`

