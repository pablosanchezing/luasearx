-- Archivo de prueba del algoritmo de búsqueda no informada DFS
local DFS = require 'LuaSearx.Uninformed_search.dfs'

----[[ INICIO PROBLEMA MISIONEROS
  
print("Problema de los misioneros y los canivales")
local handler = require 'LuaSearx.handlers.misioneros_handler'
local dfs = DFS(handler)
handler.init()

local start, goal = handler.getNode(3,3,1), handler.getNode(0,0,0)

local t1 = os.clock()
camino = dfs:findPath(start, goal)
local t2 = os.clock()

for i,x in ipairs(camino) do
  print(x)
end

print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos")

--FIN ]]--

--[[INICIO PROBLEMA JARRAS

print("Problema de las dos jarras")
local handler = require 'LuaSearx.handlers.jarras_handler'
local dfs = DFS(handler)
local map = {{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}}
handler.init(map)

local start, goal = handler.getNode(4,3), handler.getNode(2,0)

local t1 = os.clock()
camino = dfs:findPath(start, goal)
local t2 = os.clock()

for i,x in ipairs(camino) do
  print(x)
end

print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos") 

--FINAL]]--

--[[ INICIO PROBLEMA PUENTE Y LINTERNA 

--(Para este algoritmo, el tiempo de ejecución de este problema es muy elevado)

-- Función de representación
function printEstadoPuentes(nodo)
  io.write("Nodo: Operador: "..nodo.p1.." - Tiempo: "..nodo.t.." - n: {")
  for i,e in ipairs(nodo.n) do
    io.write(e)
    if  i ~= #nodo.n then
      io.write(", ") end
  end
  io.write("} nS: {")
  for i,e in ipairs(nodo.nS) do
    io.write(e)
    if  i ~= #nodo.nS then
        io.write(", ") end
  end
  io.write("} L: "..nodo.l.."\n")
end

print("Problema del puente y la linterna")
local handler = require 'LuaSearx.handlers.puente_handler'
local dfs = DFS(handler)
handler.init()

local start, goal = handler.getNode(0,0,900,{60,120,300,480},{},1), handler.getNode(7,_,0,_,_,_)

local t1 = os.clock()
camino = dfs:findPath(start, goal)
local t2 = os.clock()

print("Camino recorrido:")
for i,x in ipairs(camino) do
printEstadoPuentes(x)
end

print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos")

--FIN ]]--

--[[ INICIO PROBLEMA SUMZERO

  -- Función de representación
  function print_r(arr, indentLevel)
    local str = ""
    local indentStr = "#"
    if(indentLevel == nil) then
        print(print_r(arr, 0))
        return
    end
    for i = 0, indentLevel do
        indentStr = indentStr.."\t"
    end
    for index,value in pairs(arr) do
        if type(value) == "table" then
            str = str..indentStr..index..": \n"..print_r(value, (indentLevel + 1))
        else 
            str = str..indentStr..index..": "..value.."\n"
        end
    end
    return str
  end

    print("Problema de la suma de subconjuntos")
    local handler = require 'LuaSearx.handlers.sumzero_handler'
    local dfs = DFS(handler)
    -- Conjunto inicial
    local map = {5, -6, 3, -1, 7, 4, -2, 2, 1, -3, -4, 6, -7, -5}
    handler.init(map)
  
    --Número de elementos del subconjunto final: 14
    local start, goal = handler.getInitialNode(0), 14
  
    local t1 = os.clock()
    camino = dfs:findPath(start, goal)
    local t2 = os.clock()
  
    print("Camino recorrido:")
    for i,x in ipairs(camino) do
        print("Nodo " .. i .. " : ",x.datos[1],x.datos[2])
        print_r(x.subconjunto)
    end
  
    print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos")
  
  --]]-- --FIN