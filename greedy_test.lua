-- Archivo de prueba del algoritmo de búsqueda informada voraz

local GREEDY = require 'LuaSearx.Informed_search.greedy'

  ----[[ INICIO PROBLEMA PUENTE Y LINTERNA

  -- Función de representación
  function printEstadoPuentes(nodo)
    io.write("Nodo: Operador: "..nodo.p1.." - Tiempo: "..nodo.t.." - n: {")
    for i,e in ipairs(nodo.n) do
      io.write(e)
      if  i ~= #nodo.n then
        io.write(", ") end
    end
    io.write("} nS: {")
    for i,e in ipairs(nodo.nS) do
      io.write(e)
      if  i ~= #nodo.nS then
          io.write(", ") end
    end
    io.write("} L: "..nodo.l.."\n")
  end
  
  print("Problema del puente y la linterna")
  local handler = require 'LuaSearx.handlers.puente_handler'
  local greedy = GREEDY(handler)
  handler.init()

  local start, goal = handler.getNode(0,0,900,{60,120,300,480},{},1), handler.getNode(_,_,0,{},_,_)

  local t1 = os.clock()
  camino = greedy:findPath(start, goal)
  local t2 = os.clock()

  for i,x in ipairs(camino) do
    printEstadoPuentes(x)
  end

  print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos")

  --FIN ]]--

--[[ INICIO SUMZERO

-- Función de representación
function print_r(arr, indentLevel)
    local str = ""
    local indentStr = "#"
    if(indentLevel == nil) then
        print(print_r(arr, 0))
        return end
    for i = 0, indentLevel do
        indentStr = indentStr.."\t"
    end
    for index,value in pairs(arr) do
        if type(value) == "table" then
            str = str..indentStr..index..": \n"..print_r(value, (indentLevel + 1))
        else 
            str = str..indentStr..index..": "..value.."\n"
        end
    end
    return str
  end
  
    print("Problema de la suma de subconjuntos")
    local handler = require 'LuaSearx.handlers.sumzero_handler'
    local greedy = GREEDY(handler)
    -- Conjunto inicial
    local map = {23,2,53,65,23,-1,-5,-7,-60,-90,3,80,1,40,-3,4,-4,30,21,-28,-19,-2,-78,-22,-67}
    handler.init(map)
  
    
    local start, goal = handler.getInitialNode(0), handler.getInitialNode(0)

    --Número de elementos del subconjunto final
    goal.datos[1]=14
    -- Suma de los elementos
    goal.datos[2]=0

    local t1 = os.clock()
    camino = greedy:findPath(start, goal)
    local t2 = os.clock()
  
    print("Camino recorrido:")
    for i,x in ipairs(camino) do
        print("Nodo " .. i .. " : Elementos subconjunto: "..x.datos[1].." Suma total: "..x.datos[2])
        print_r(x.subconjunto)
    end

    print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos")
    
    --FIN ]]--