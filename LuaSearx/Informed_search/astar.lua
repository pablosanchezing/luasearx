-- Implementación del algoritmo A* genérico (A star)

-- Para ser ejecutado deberá ser inicializado con un handler, que actuará de
-- interfaz entre el algortimo y el espacio de búsqueda

-- Esta implementación hace uso de una estructura de datos Binary Heap para los nodos

-- El handler para inicializar el algoritmo deberá definir las siguientes funciones:
-- handler.getNode(...)   ->  devuelve un nodo (instancia de node.lua)
-- handler.distance(a, b)  -> función heurística que devuelve la distancia entre el nodo a y el nodo b
-- handler.getNeighbors(n) -> devuelve una tabla que almacena todos los sucesores del nodo n

-- Importamos los archivos class y bheap para implementar el uso de la clase del algoritmo
-- y de el montículo de nodos, respectivamente
local class = require 'LuaSearx.utils.class'
local bheap = require 'LuaSearx.utils.bheap'

-- La función resetForNextSearch limpia la información de cada nodo (incluidos sus valores f, g y h), el montículo de nodos y la lista de nodos visitados.
-- La utilizaremos para limpiar los datos de nodos antes de comenzar una nueva búsqueda.
local function resetForNextSearch(astar)
  for node in pairs(astar.visited) do
    node.parent, node.opened, node.closed = nil, nil, nil
    node.f, node.g, node.h = 0, 0, 0
  end
  astar.open:clear()  
  astar.visited = {}
end

-- Builds and returns the path to the goal node
local function backtrace(node)
  local path = {}
  repeat
    table.insert(path, 1, node)
    node = node.parent
  until not node
  return path
end

-- La función isGoal comprueba si node es un estado solución cuando este se trata de una tabla
-- compuesta de tablas
function isGoal(node,goal)
  if type(node.n) == type({}) and #node.n == 0 then return true end
  if type(node.subconjunto) == type({}) and node.datos[1] == goal.datos[1] and node.datos[2] == goal.datos[2] then return true end
    return false
end

-- Definimos la clase del algoritmo A*
local Astar = class()

-- Esta función inicializa el algoritmo con el handler seleccionado, el montículo binario
-- (lo llamaremos open y almacenará los nodos abiertos), la función heurística y la lista de nodos visitados (cerrados) vacía
function Astar:initialize(handler)
  self.handler = handler
  self.open = bheap()
  self.heuristic = handler.distance
  self.visited = {}
end

-- Returns the path between start and goal locations
-- start  : a Node representing the start location
-- goal   : a Node representing the target location
-- returns: an array of nodes

-- La función findPath es la principal del algoritmo y a la que llamaremos para encontrar la
-- solución del problema y el camino recorrido. Sus argumentos son el nodo inicial (start)
-- y el nodo objetivo que consideramos solución (goal)
function Astar:findPath(start, goal)
  -- Limpiamos los datos de los nodos de búsquedas anteriores
  resetForNextSearch(self)
  -- Calculamos las funciones g, h y f del nodo inicial, lo introducimos en la lista de nodos abiertos
  -- y lo marcamos como visitado en la lista de nodos visitados
  start.g = 0
  start.h = self.heuristic(start, goal)
  start.f = start.g + start.h
  self.open:push(start)
  self.visited[start] = true
  -- Con este bucle recorremos todo los estados necesarios hasta dar con la solución.
  -- Extraemos un nodo de la lista de nodos abiertos, comprobamos si es un nodo solución (en ese caso construimos
  -- el camino y lo devolvemos), marcamos el nodo como cerrado y generamos cada uno de los nodos hijo 
  -- (que llamamos vecinos) del nodo actual
  while not self.open:isEmpty() do
    local node = self.open:pop()

    --Es posible que sea necesario modificar esta línea para lograr una mejor adaptación de su problema
    if node == goal or isGoal(node,goal) then return backtrace(node) end

    node.closed = true
    local neighbors = self.handler.getNeighbors(node)
    -- A continuación recorremos la lista de vecinos actualizando la información de cada uno de ellos,
  -- si no están marcados como cerrados:
  -- ·Calculamos la función g desde el nodo actual hasta el vecino (con la función heurística)
  -- Después, si no se encuentran en la lista de nodos abiertos o la función g calculada es menor al a la 
  -- función g del vecino:
  -- ·Definimos quién es su nodo padre (nodo actual)
  -- ·Actualizamos el valor de la función g, h y f
  -- ·Lo marcamos como visitado en la lista de nodos visitados.
    for _, neighbor in ipairs(neighbors) do
      if not neighbor.closed then
        local tentative_g = node.g + self.heuristic(node, neighbor)
        if not neighbor.opened or tentative_g < neighbor.g then
          neighbor.parent = node
          neighbor.g = tentative_g
          neighbor.h = self.heuristic(neighbor, goal)
          neighbor.f = neighbor.g + neighbor.h
          self.visited[neighbor] = true
          -- Además, si no se encuentra en la lista de nodos abiertos, lo marcamos como abierto y lo introducimos
          -- en la lista de nodos abiertos. De lo contrario, reordenamos el nodo dentro de la lista de nodos abiertos
          if not neighbor.opened then
            neighbor.opened = true
            self.open:push(neighbor)
          else
            self.open:sort(neighbor)
          end
        end
      end
    end
  end
end

return Astar