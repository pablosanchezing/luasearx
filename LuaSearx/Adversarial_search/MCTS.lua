-- Implementación del algoritmo Monte Carlo Tree Search (UCT) - (MCTS-UCT)

local class = require 'LuaSearx.utils.class'

-- Definimos la clase del algoritmo MCTS
local MCTS = class()

-- Esta función inicializa el algoritmo con el handler seleccionado
function MCTS:initialize(handler)
  self.handler = handler
end

-- Declaramos la variable que almacenará el valor de la constante Cp en la función UCTSelectChild
Cp = (1/math.sqrt(2))

-- La función update se encarga de actualizar las visitas y las victorias del nodo (node) a partir del valor de la recompensa (reward)
-- y el jugador que la ha obtenido (player)
function updateNode(node,reward,player)
    node.visits = node.visits + 1
    if node.state[2] == player then
        node.wins = node.wins + reward
    end
end

-- La función ascends se encarga de actualizar las visitas y victorias del nodo (node) dentro de su nodo padre y devuelve al
-- nodo padre
function ascends(node)
  if node == nil or node.parent == nil then
      return nil
  end
-- Almacenamos en parent al nodo padre de node, que devolveremos al final de la función
  local parent = node.parent
-- Con este bucle realizamos la búsqueda de node entre todos los hijos de parent y actualizamos las visitas y victorias
-- que su hijo ha obtenido
  for i,child in pairs(parent.childs) do
      if child.rule == node.rule then
          child.visits = node.visits
          child.wins = node.wins
      end
  end
  return parent
end

-- La función updateRoot se encarga de actualizar todas las visitas de los nodos hijos del nodo raíz (rootNode)
-- a partir de los datos del nodo actual (node, que representará al nodo raíz después de ejecutar cada una de las 
-- fases del algoritmo)
function updateRoot(rootNode,node)
  -- En primer lugar, actualizamos sus propias visitas (aumentándolas en 1)
  rootNode.visits = rootNode.visits + 1
    -- Recorremos la lista de hijos del nodo raíz por cada hijo del nodo actual para localizar a los semejantes y
    -- realizar su correspondiente actualización de visitas y victorias en rootState
  for i,childC in pairs(node.childs) do
    for k,childR in pairs(rootNode.childs) do
        if childC.rule == childR.rule then
            rootNode.childs[k].visits = node.childs[i].visits
            rootNode.childs[k].wins = node.childs[i].wins
        end
    end
  end
end

-- La función UCTSelectChild se encarga de seleccionar al hijo del nodo (node) que maximice la fórmula de UCT
function UCTSelectChild (node)
    -- Almacenamos las visitas del nodo y creamos las variables que almacenarán el valor máximo y su respectivo 
    -- nodo hijo
    local visits = node.visits
    local maxValue = 0
    local maxChild = node.childs[1]
    -- A continuación, recorremos la lista de hijos de node. Para cada uno de ellos, calculamos la fórmula de UCT
    -- y, si el valor obtenido es el mayor hasta el momento, se actualizan las variables maxValue y maxChild
    for i,child in pairs(node.childs) do
        local currentValue = ((child.wins / child.visits) + 2*Cp*(math.sqrt(2 * math.log(visits) / child.visits)))
        if currentValue > maxValue then
            maxValue = currentValue
            maxChild = child
        end
    end
    -- Finalmente devolvemos el hijo correspondiente al mayor valor obtenido
    return maxChild
end

-- La función UCT es la función principal del algoritmo, que ejecutará cada una de las fases del MCTS-UCT.
-- Será la función a la que llamemos para obtener la mejor acción a partir del estado inicial (roorState) y realizando
-- un número de iteraciones igual a iter
function MCTS:UCT (rootState,iter)

    -- En primer lugar, creamos el nodo correspondiente al estado inicial (será el nodo raíz) y comenzamos el bucle
    -- de iteraciones que ejecutará cada una de las fases
    local rootNode = self.handler.createNode(rootState)
    for i=1, iter do

      if type(rootState[1]) == type({}) then
        rootNode.state[1] = copyTable(rootState[1])
        rootNode.state[2] = rootState[2]
      end

      -- Almacenamos al nodo raíz (rootNode) en la variable cNode (que representará al nodo actual)
      local cNode = rootNode

      -- FASE DE SELECCIÓN
      -- Con este bucle iremos seleccionando el nodo hijo más urgente en cada momento (haciendo uso de la función
      -- UCTSelectChild) hasta alcanzar un nodo
      -- que no esté completamente expandido ---que tiene reglas disponibles--- o que represente a un estado final
      -- del juego ---que no tenga hijos---
      while #cNode.untriedRules == 0 and #cNode.childs ~= 0 do
        cNode = UCTSelectChild(cNode)
      end
      -- FASE DE EXPANSIÓN
      -- A continuación, si el nodo actual tiene reglas disponibles (no era un nodo terminal), añadimos un nuevo
      -- sucesor (haciendo uso de la función AddChild definida en el handler del juego) mediante una de sus reglas elegida aleatoriamente
      if #cNode.untriedRules ~= 0 then
        local r = cNode.untriedRules[math.random(#cNode.untriedRules)]
        cNode = self.handler.AddChild(cNode,r)
      end
      -- FASE DE SIMULACIÓN
      -- Declaramos la variable s, que almacenará cada uno de los que iremos recorriendo durante la simulación, 
      -- y comenzará con el estado del nodo actual.
      local s = nil
      if (type(cNode.state[1])) == type({}) then
            s = {}
            s[1] = copyTable(cNode.state[1])
            s[2] = cNode.state[2]
      else
            s = cNode.state
      end
      -- Mientras queden reglas disponibles,
      -- vamos aplicando una regla a cada estado (aleatoriamente de entre todas las disponibles) mediante las funciones
      -- getRules y apply definidas en el handler del juego. De esta forma realizamos una simulación completa del juego
      -- quedando almacenado en s su estado final
      while #self.handler.getRules(s) ~= 0 do
        local r = self.handler.getRules(s)[(math.random(#self.handler.getRules(s)))]
        s = self.handler.apply(r,s)
      end
      -- FASE DE ACTUALIZACIÓN
      -- Para realizar la retropropagación, comenzamos declarando las variables reward y player, que representarán
      -- la recompensa obtenida en el estado final de la simulación y el jugador ganador, respectivamente.
      local reward = self.handler.getResult(s,self.handler.getPlayerJustMoved(cNode.state))
      local player = cNode.state[2]
      --  Mientras el nodo actual no sea nulo (es decir, mientras no hayamos alcanzado el nodo raíz):
      -- ·Comprobamos si el nodo actual es el nodo raíz (su padre sera nulo) y en ese caso
      -- actualizamos las visitas y victorias de rootNode mediante la función updateRoot
      while cNode ~= nil do
        if cNode.parent == nil then
            updateRoot(rootNode,cNode)
        -- Si se trata de cualquier otro nodo del árbol, actualizamos sus visitas y victorias mediante la función
        -- updateNode (haciendo uso de las variables reward y player definidas previamente)
        else
            updateNode(cNode,reward,player)
        end
        -- Finalmente, haciendo uso de la función ascends, ascendemos por el árbol (actualizando las visitas y victorias 
        -- del nodo actual en su nodo padre) almacenando en cNode a su nodo padre
        cNode = ascends(cNode)
      end
    end

    -- Para devolver el mejor movimiento, declaramos las variables maxVisits y maxChild, que contendrán el mayor 
    -- número de visitas
    -- y el hijo correspondiente del rootNode, respectivamente
    local maxVisits = 0
    local maxChild = nil
    --printeaListaChilds(rootNode,1)

    -- Recorremos la lista de hijos del nodo raíz y nos quedamos con aquel que haya sido más visitado 
    -- (almacenándolo en maxChild). Para finalizar, devolvemos la regla asociada al mejor sucesor obtenido
    for i,child in pairs(rootNode.childs) do
        if child.visits > maxVisits then
            maxVisits = child.visits
            maxChild = child
        end
    end
    return maxChild.rule
end

function printeaListaChilds(node,iter)
    io.write("Iter: "..iter..". Node childs: ")
    for i,child in pairs (node.childs) do
        io.write(","..child.rule.." ("..child.visits..") ("..child.wins..") ")
    end
    io.write("\n")
end

function printeaNodo(node)
    io.write("UntriedRules: "..#node.untriedRules..". Node childs: ")
    for i,child in pairs (node.childs) do
        io.write(","..child.rule.." ("..child.visits..") ")
    end
    io.write("\n")
end

return MCTS