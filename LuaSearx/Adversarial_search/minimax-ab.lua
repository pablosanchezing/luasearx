-- Implementación del algoritmo de búsqueda Minimax con Poda alfa-beta

local class = require 'LuaSearx.utils.class'

-- Definimos e inicializamos la clase del algoritmo Minimax con Poda alfa-beta
local MinimaxAB = class()
function MinimaxAB:initialize(handler)
end

-- La función findChoice es la función principal del algoritmo. Se trata de una función recursiva que 
-- encontrará la mejor acción a partir del estado inicial (state) comenzando por un nivel MIN o MAX (maximize)
-- y haciendo uso de un handler (handler). Los valores iniciales para el jugador MAX serán 
-- (alpha,beta) = (+\infinite,-\infinite).
function MinimaxAB:findChoiceAB(state, maximize, handler, alpha, beta)
  -- Creamos el nodo correspondiente al estado actual y generamos todos sus hijos. Comprobamos si el nodo actual es
  -- final (si es así, devolvemos el valor del resultado del juego) y almacenamos en children a los hijos del nodo actual
  local node = handler.createNode(state)

  handler.generatesChilds(node)
  if node.final == true then

    return {"valor",handler.getResult(node.state,2)}
  end
  local children = node.childs
    --  Si la variable booleana maximize es true 
    -- (lo que significa que es el turno del jugador MAX)
  if maximize then
      -- Recorremos cada uno de los hijos del nodo actual. Actualizaremos la variable
      -- alpha con el valor máximo entre su valor actual y el resultado de llamar de nuevo a la función findChoice.
      -- Esta recursión construirá y recorrerá el resto del árbol hasta alcanzar un nodo final y, entonces,
      -- se resolverán cada una de las llamadas recursivas hasta llegar al nodo actual
    local rule = nil
    for i, child in ipairs(children) do

      local maxBestScore = alpha

      alpha = math.max(alpha, MinimaxAB:findChoiceAB(child.state, false, handler, alpha, beta, rule)[2])
      -- Comprobamos si el valor de beta es menor o igual que el valor de alpha. En ese caso podamos esta rama del árbol 
      -- (paramos el bucle)

      if maxBestScore < alpha then 
        rule = child.rule
      end

      if beta <= alpha then break end
    end
    -- Finalmente devolvemos una dupla que contiene la regla seleccionada y el valor de alpha (el máximo alcanzado)
    return {rule,alpha}
      -- Si la variable booleana maximize es false (lo que significa que es el turno del jugador MIN), 
      -- entonces se realizará la mismo que en el caso de MAX pero esta vez actualizando la variable beta
      -- con el valor mínimo entre su valor actual y el resultado de llamar de nuevo a la función findChoice.
  else
    local rule = nil
    for i, child in ipairs(children) do

      local minBestScore = beta

      beta = math.min(beta, MinimaxAB:findChoiceAB(child.state, true, handler, alpha, beta, rule)[2])

      if minBestScore < beta then 
        rule = child.rule
      end

      if beta <= alpha then break end
    end
    return {rule,beta}
  end
end

-- Funciones de representación de estados

function imprimeEstadoTictactoe (posiciones)
  io.write("Posiciones:")
  for i,pos in ipairs(posiciones) do
    io.write(", "..pos)
  end
  io.write("\n")
end

function representaTablero (t)
  cas={}
  for i,pos in pairs(t) do
      if     pos == 0 then cas[i]=" "
      elseif pos == 1 then cas[i]="X"
      else                 cas[i]="O"
      end
  end
print("\n")
print("    "..cas[1].." | "..cas[2].." | "..cas[3].." ")
print("    ---------")
print("    "..cas[4].." | "..cas[5].." | "..cas[6].." ")
print("    ---------")
print("    "..cas[7].." | "..cas[8].." | "..cas[9].." ")
print("\n")
end

-------------------------------------------------------------------------------------------------------------

return MinimaxAB
