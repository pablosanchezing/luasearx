-- Implementación del algoritmo de búsqueda Minimax

local class = require 'LuaSearx.utils.class'

local Minimax = class()
function Minimax:initialize(handler)
end

-- La función findChoice es la función principal del algoritmo. Se trata de una función recursiva 
-- que encontrará la mejor acción a partir del estado inicial (state) comenzando por un nivel
-- MIN o MAX (maximize) y haciendo uso de un handler almacenado en la variable handler\\
function Minimax:findChoice(state, maximize, handler)
  -- Creamos el nodo correspondiente al estado actual y generamos todos sus hijos. 
  -- Comprobamos si el nodo actual es final (si es así, devolvemos el valor del resultado del juego) 
  -- y almacenamos en children a los hijos del nodo actual
  local node = handler.createNode(state)
  handler.generatesChilds(node)
  if node.final == true then
    return {"valor",handler.getResult(node.state,2)}
  end

  local children = node.childs
  -- Si la variable booleana maximize es true (lo que significa que es el turno 
  -- del jugador MAX, declaramos la variable bestScore con valor -infinito 
  -- (representará el mejor valor del nivel actual) y la variable local rule (almacenará la regla 
  -- correspondiente al estado con mejor valor en cada momento)
  if maximize then
    bestScore = -math.huge
    local rule = nil
    -- A continuación, recorremos cada uno de los hijos del nodo actual. Declaramos la variable local maxBestScore, 
    -- que almacenará el valor máximo actual de la variable bestScore antes de que pueda ser cambiado. 
    -- Ahora sí, actualizaremos la variable bestScore con el valor máximo entre su valor actual y el resultado 
    -- de llamar de nuevo a la función findChoice. Esta recursión construirá y recorrerá el resto del árbol 
    -- hasta alcanzar un nodo final y, entonces, se resolverán cada una de las llamadas recursivas hasta llegar al 
    -- nodo actual. Después, comprobamos si el valor de bestScore ha mejorado y, si es así, actualizamos el 
    -- valor de rule. Para terminar devolvemos una dupla que contiene la regla seleccionada y el valor de 
    -- bestScore (el máximo alcanzado)
    for i, child in ipairs(children) do

      local maxBestScore = bestScore

      bestScore = math.max(bestScore, Minimax:findChoice(child.state, false, handler, rule)[2])

      if maxBestScore < bestScore then 
        rule = child.rule
      end

    end
    return {rule,bestScore}
    -- Si la variable booleana maximize es false (lo que significa que es el turno del jugador MIN),
    -- entonces se realizará la mismo que en el caso de MAX pero esta vez almacenando en la variable bestScore
    -- el valor mínimo (representará el mejor valor del nivel actual, comenzando con un valor de infinito) y su valor mínimo 
    -- actual en minBestScore
  else
    bestScore = math.huge
    local rule = nil
    for i, child in ipairs(children) do

      local minBestScore = bestScore

      bestScore = math.min(bestScore, Minimax:findChoice(child.state, true, handler, rule)[2])

      if minBestScore > bestScore then 
        rule = child.rule
      end

    end
    return {rule,bestScore}
  end
end

-- Funciones de representación de estados

function imprimeEstadoTictactoe (posiciones)
  io.write("Posiciones:")
  for i,pos in ipairs(posiciones) do
    io.write(", "..pos)
  end
  io.write("\n")
end

function representaTablero (t)
  cas={}
  for i,pos in pairs(t) do
      if     pos == 0 then cas[i]=" "
      elseif pos == 1 then cas[i]="X"
      else                 cas[i]="O"
      end
  end
print("\n")
print("    "..cas[1].." | "..cas[2].." | "..cas[3].." ")
print("    ---------")
print("    "..cas[4].." | "..cas[5].." | "..cas[6].." ")
print("    ---------")
print("    "..cas[7].." | "..cas[8].." | "..cas[9].." ")
print("\n")
end

return Minimax