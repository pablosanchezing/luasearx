-- Implementación del Problema de la suma de subconjuntos

-- Como en cada uno de los problemas, importamos el archivo node para 
-- implementar el uso de los nodos
local Node = require ('LuaSearx.utils.node')

-- Implements Node class (from node.lua)
function Node:initialize() 
  self.datos = {0,0}
  self.subconjunto = {}
end

-- Definimos las funciones initialize, toString y isEqualTo, que indicarán la forma, la representación y la función
-- de comparación de cada uno de los nodos del problema
function Node:initialize() 
  self.datos = {0,0}
  self.subconjunto = {}
end
function Node:toString() return ('Node: x = %d, y = %d'):format(self.datos[1], self.datos[2]) end
function Node:isEqualTo(n) return self.datos[1] == n.datos[1] and 
                                    self.datos[2] == n.datos[2] and 
                                      self.subconjunto == n.subconjunto end

-- Declaramos la tabla handler, que será la tabla devuelta por este
-- archivo y permitirá acceder a los datos y funciones del problema
local handler = {}

-- La función init se encarga de inicializar el espacio de estados. Almacenará en el handler el parámetro que representa al
-- conjunto entrada del problema
function handler.init(conjunto)
  handler.conjunto = conjunto
end

-- La función getInitialNode se encarga de devolver al nodo inicial del problema, inicializando sus atributos
function handler.getInitialNode()
  local node = Node()
  node.datos = {0,0}
  node.subconjunto = {}
  return node
end

-- La función distance se encarga de devolver la distancia Manhattan entre los nodos a y b. Será utilizada en
-- los algoritmos de búsqueda con heurística. En este caso la distancia será la diferencia de tamaño entre los subconjuntos
-- de cada nodo
function handler.distance(a, b)
  --if b[3]==0 then return b[2] - a.datos[2] end
  local distance = b.datos[2] - a.datos[2] 
  return distance
end

-- La función getNeighbors se encarga de calcular cada uno de los nodos sucesores (hijos) del
-- nodo node y devuelve una lista con todos ellos
function handler.getNeighbors(node)
  local neighbors = {}
  -- Recorremos cada elemento del conjunto del problema, declaramos al nodo vecino (o nodo sucesor de node) como
  -- una copia de node (mediante la función copyNode, que realiza una copia de node y la devuelve)
  -- y, si el elemento actual no está en el subconjunto del nodo actual (node), lo introducimos, actualizamos sus
  -- atributos (mediante la función totalSum, que realiza la suma de los elementos del conjunto) y añadimos 
  -- el nuevo nodo a la lista de vecinos
  for i,e in ipairs(handler.conjunto) do
    local vecino = copyNode(node)
    if not contains(node.subconjunto,e) then
      table.insert(vecino.subconjunto,e)
      vecino.datos = {#vecino.subconjunto, totalSum(vecino.subconjunto)}
      table.insert(neighbors, vecino)
    end
  end
  -- Finalmente devolvemos la lista de todos los nodos hijo de n
  return neighbors
end

function copyNode(node)
  local newNode = Node()
  local subconjunto = {}
  for i,e in ipairs(node.subconjunto) do
    table.insert(subconjunto,e)
  end
  newNode.subconjunto = subconjunto
  newNode.datos = {#newNode.subconjunto, totalSum(newNode.subconjunto)}
return newNode
end


function totalSum(lista)
  sum = 0
  for i,e in ipairs(lista) do
    sum = sum + e
  end
return sum
end

function contains(list, x)
	for _, v in pairs(list) do
		if v == x then return true end
	end
	return false
end

return handler
