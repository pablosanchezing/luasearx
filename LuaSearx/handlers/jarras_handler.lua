-- Implementación del Problema de las 2 jarras

-- Como en cada uno de los problemas, importamos el archivo node para 
-- implementar el uso de los nodos
local Node = require ('LuaSearx.utils.node')

-- Definimos las funciones initialize, toString y isEqualTo, que indicarán la forma, la representación y la función
-- de comparación de cada uno de los nodos del problema
function Node:initialize(x, y) self.x, self.y = x, y end
function Node:toString() return ('Node: x = %d, y = %d'):format(self.x, self.y) end
function Node:isEqualTo(n) return self.x == n.x and self.y == n.y end

-- Declaramos las tablas nodes, que almacenará todos los nodos del problema, y handler, que será la tabla devuelta por este
-- archivo y permitirá acceder a los datos y funciones del problema
local nodes = {}
local handler = {}

-- La función init se encarga de inicializar el espacio de estados. Introducirá en nodes todos los posibles 
-- nodos del problema haciendo uso de un map (su tamaño definirá cuáles serán los estados del problema) de dos
-- dimensiones (válido para este caso)
function handler.init(map)
  handler.map = map
  nodes = {}
  for y, line in ipairs(map) do
    for x in ipairs(line) do
      table.insert(nodes, Node(x-1, y-1))
    end
  end
end

-- La función getAllNodes devuelve la tabla nodes
function handler.getAllNodes() return nodes end

-- La función getNode se encarga de devolver al nodo (de la tabla nodes) correspondiente dado su estado.
-- Calculará el índice del nodo en función del estado (determinado por x e y) y devolverá al nodo en cuestión
function handler.getNode(x, y)
  local h, w = #handler.map, #handler.map[1]
  local k = y * w + x + 1
  return nodes[k]
end

-- La función distance se encarga de devolver la distancia Manhattan entre los nodos a y b. Será utilizada en
-- los algoritmos de búsqueda con heurística
function handler.distance(a, b)
  local dx, dy = a.x - b.x, a.y - b.y
  return math.abs(dx) + math.abs(dy)
end

--Pasamos ahora a definir las funciones que representarán a cada uno de los operadores del problema

--Llenar la jarra de 4 litros con el grifo

function llenar1(nodo)
  return 4,nodo.y
end

--Llenar la jarra de 3 litros con grifo

function llenar2(nodo)
  return nodo.x,3
end

--Vaciar la jarra de 4 litros en el suelo.

function vaciar1(nodo)
  return 0,nodo.y
end

--Vaciar la jarra de 3 litros en el suelo

function vaciar2(nodo)
  return nodo.x,0
end

--Llenar la jarra de 4 litros con la jarra de 3 litros

function llenar_12(nodo)
  local j1 = nodo.x
  local j2 = nodo.y
  while (j1 < 4) and (j2 > 0) do
      j2 = j2-1
      j1 = j1+1
  end
  return j1,j2
end

--Llenar la jarra de 3 litros con la jarra de 4 litros

function llenar_21(nodo)
  local j1 = nodo.x
  local j2 = nodo.y
  while (j2 < 3) and (j1 > 0) do
      j1 = j1-1
      j2 = j2+1
  end
  return j1,j2
end

--Vaciar la jarra de 3 litros en la jarra de 4 litros

function vaciar_12(nodo)
  local j1 = nodo.x
  local j2 = nodo.y
  if (j1+j2) > 4 then
      return 4,0
  end
  return j1+j2,0
end

--Vaciar la jarra de 4 litros en la jarra de 3 litros

function vaciar_21(nodo)
  local j1 = nodo.x
  local j2 = nodo.y
  if (j1+j2) > 3 then
      return 0,3
  end
  return 0,j1+j2
end

-- La función getNeighbors se encarga de calcular cada uno de los nodos sucesores (hijos) del
-- nodo n y devuelve una lista con todos ellos
function handler.getNeighbors(n)
  local neighbors = {}
  local x, y = n.x, n.y
  nodo=Node(x, y)
  -- Aplicamos cada uno de los operadores e insertamos el nodo resultante en la lista
  vx,vy = llenar1(nodo)
  table.insert(neighbors, handler.getNode(vx,vy))
  vx,vy = llenar2(nodo)
  table.insert(neighbors, handler.getNode(vx,vy))
  vx,vy = vaciar1(nodo)
  table.insert(neighbors, handler.getNode(vx,vy))
  vx,vy = vaciar2(nodo)
  table.insert(neighbors, handler.getNode(vx,vy))
  vx,vy = llenar_12(nodo)
  table.insert(neighbors, handler.getNode(vx,vy))
  vx,vy = llenar_21(nodo)
  table.insert(neighbors, handler.getNode(vx,vy))
  vx,vy = vaciar_12(nodo)
  table.insert(neighbors, handler.getNode(vx,vy))
  vx,vy = vaciar_21(nodo)
  table.insert(neighbors, handler.getNode(vx,vy))
  -- Finalmente devolvemos la lista de todos los nodos hijo de n
  return neighbors
end

return handler