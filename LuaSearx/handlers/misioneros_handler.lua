-- Implementación del Problema de Misioneros y caníbales

-- Como en cada uno de los problemas, importamos el archivo node para 
-- implementar el uso de los nodos
local Node = require ('LuaSearx.utils.node')

-- Definimos las funciones initialize, toString y isEqualTo, que indicarán la forma, la representación y la función
-- de comparación de cada uno de los nodos del problema
function Node:initialize(m, c, b) self.m, self.c, self.b = m, c, b end
function Node:toString() return ('Node: m = %d, c = %d, b = %d'):format(self.m, self.c, self.b) end
function Node:isEqualTo(n) return self.m == n.m and self.c == n.c and self.b == n.b end

--[[

Estado -> ( m , c , b )

m -> número de misioneros en la orilla derecha
c -> número de caníbales en la orilla derecha
b -> '0' si la barca está en la orilla derecha y '1' si está en la izquierda

]]--

-- Declaramos las tablas nodes, que almacenará todos los nodos del problema, y handler, que será la tabla devuelta por este
-- archivo y permitirá acceder a los datos y funciones del problema
local nodes = {}
local handler = {}

-- La función init se encarga de inicializar el espacio de estados. Introducirá en nodes todos los posibles 
-- nodos del problema. En esta ocasión, debido al reducido tamaño del problema, hemos podido añadir manualmente cada
-- uno de los nodos a la lista, pero en caso de tratarse de un problema mayor, tendríamos que recurrir al cálculo
-- de todos ellos mediante un bucle.
function handler.init()
  nodes = {
    Node(3,3,1),
    Node(3,3,0),
    Node(2,2,1),
    Node(2,2,0),
    Node(1,1,1),
    Node(1,1,0),
    Node(3,0,1),
    Node(3,0,0),
    Node(2,0,1),
    Node(2,0,0),
    Node(1,0,1),
    Node(1,0,0),
    Node(0,3,1),
    Node(0,3,0),
    Node(0,2,1),
    Node(0,2,0),
    Node(0,1,1),
    Node(0,1,0),
    Node(3,1,0),
    Node(3,1,1),
    Node(2,1,0),
    Node(2,1,1),
    Node(3,2,0),
    Node(3,2,1),
    Node(0,0,0)}
end

-- La función getAllNodes devuelve la tabla nodes
function handler.getAllNodes() return nodes end

-- La función getNode se encarga de devolver al nodo (de la tabla nodes) correspondiente dado su estado.
-- Calculará el índice del nodo en función del estado (determinado por x e y) y devolverá al nodo en cuestión.
function handler.getNode(m, c, b)
  for index, node in pairs(nodes) do
    if node:isEqualTo(Node(m,c,b)) then
      return nodes[index]
    end
  end
end

-- La función distance se encarga de devolver la distancia Manhattan entre los nodos a y b. Será utilizada en
-- los algoritmos de búsqueda con heurística
function handler.distance(a, b)
  local dm, dc = a.m - b.m, a.c - b.c
  return math.abs(dm) + math.abs(dc)
end

-- La función check se encarga de comprobar si un estado cumple con los requisitos del problema
function check(m,c,b)
  --Comprueba que no hay más caníbales que misioneros en ninguna orilla
  if ((3-m) >= (3-c) or m==3) and (m>=c or m==0)  then
    --Comprueba que hay entre 3 y 0 misioneros y caníbales en la orilla derecha
    if m<4 and m>=0 and c<4 and c>=0 then
      return true
    end
  end
  return false
end 

--Pasamos ahora a definir las funciones que representarán a cada uno de los operadores del problema

--Dos misioneros cruzan el río

function MM(nodo,direccion)
  if direccion == true then
      return {nodo.m -2, nodo.c, 0}
  else
      return {nodo.m +2, nodo.c, 1}
  end
end

--Dos caníbales cruzan el río

function CC(nodo,direccion)
  if direccion == true then
    return {nodo.m, nodo.c -2, 0}
  else
    return {nodo.m,nodo.c +2, 1}
  end
end

--Un misionero y un caníbal cruzan el río

function MC(nodo,direccion)
  if direccion == true then
    return {nodo.m -1, nodo.c -1, 0}
  else
    return {nodo.m +1,nodo.c +1, 1}
  end
end

--Un misionero cruza el río

function M(nodo,direccion)
  if direccion == true then
    return {nodo.m -1, nodo.c, 0}
  else
    return {nodo.m +1,nodo.c, 1}
  end
end

--Un caníbal cruza el río

function C(nodo,direccion)
  if direccion == true then
    return {nodo.m, nodo.c -1, 0}
  else
    return {nodo.m,nodo.c +1, 1}
  end
end


-- La función getNeighbors se encarga de calcular cada uno de los nodos sucesores (hijos) del
-- nodo n y devuelve una lista con todos ellos
function handler.getNeighbors(n)
  local neighbors = {}
  local m, c, b = n.m, n.c, n.b
  nodo=Node(m, c, b)
  -- Comprobamos el lado del río en que se encuentra la barca. Si está en el lado izquierdo (b==1), aplicamos cada 
  -- uno de los operadores que parten desde ahí e insertamos el nodo resultante en la lista (tras comprobar que el
  -- estado resultante cumple las reglas del problema mediante la función check)
  if b==1 then
    if check(m-2, c, 0) then
      v = MM(nodo,true)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
    if check(m, c-2, 0) then
      v = CC(nodo,true)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
    if check(m-1, c-1, 0) then
      v = MC(nodo,true)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
    if check(m-1, c, 0) then
      v = M(nodo,true)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
    if check(m, c-1, 0) then
      v = C(nodo,true)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
  return neighbors
  end
  if b==0 then
  -- En caso de que se encuentre en el lado derecho, aplicamos los mismos operadores en el sentido opuesto
    if check(m+2, c, 1) then
      v = MM(nodo,false)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
    if check(m, c+2, 1) then
      v = CC(nodo,false)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
    if check(m+1, c+1, 1) then
      v = MC(nodo,false)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
    if check(m+1, c, 1) then
      v = M(nodo,false)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
    if check(m, c+1, 1) then
      v = C(nodo,false)
      table.insert(neighbors, handler.getNode(v[1],v[2],v[3]))
    end
  -- Finalmente devolvemos la lista de todos los nodos hijo de n
  return neighbors
  end
end

return handler