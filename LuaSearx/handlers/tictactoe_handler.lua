-- Implementación del juego Tic Tac Toe

-- Como en cada uno de los juegos, importamos el archivo node para 
-- implementar el uso de los nodos
local Node = require ('LuaSearx.utils.node')

-- Declaramos la tabla handler, que será la tabla devuelta por este
-- archivo y permitirá acceder a los datos y funciones del juego
local handler = {}

-- La función initialize se encarga de declarar los atributos de los que dispondrá cada uno
-- de los nodos del juego y cual será su valor inicial. Los atributos serán el nodo padre, la lista
-- de nodos hijo, el estado, las victorias y visitas acumuladas, la regla aplicada para alcanzar al nodo, 
-- la lista de reglas disponibles y un booleano que nos indica si el estado es final (true en ese caso)
function Node:initialize(state)
    self.parent = nil
    self.childs = {}
    -- Almacenaremos temporalmente un valor cualquiera en el atributo state y a continuación lo modificamos
    -- por el valor del contenido del problema (haciendo uso de la función copyTableInit, que se encargará de realizar
    -- una copia del primer elemento del estado y devolverla, evitando así la mutabilidad de la tabla que representa dicho
    -- contenido durante la ejecución del algoritmo). De la misma forma almacenaremos el segundo elemento del estado
    -- (correspondiente al jugador) mediante la función copyValue, que se encargará de extraer y devolver el valor de state
    self.state = {1,2}
    self.state[1] = copyTableInit(state)
    self.state[2] = copyValue(state)
    self.wins = 0
    self.visits = 0
    self.rule = 0
    self.untriedRules = handler.getRules(state)
    self.final = (handler.getResult(state,state[2]) ~= false)
end

-- La función createNode se encarga de llamar a la función initialize con el estado state
function handler.createNode(state) return Node(state) end

--function handler.init()
    --print("Inicializando handler")
  --end

function copyTableInit(origin)
    if origin == nil then
        return {}
    end
    tabla = {}
    for i,e in ipairs(origin[1]) do
        table.insert(tabla,e)
    end
    return tabla
end

function copyTable(origin)
    if origin == nil then
        return {}
    end
    tabla = {}
    for i,e in ipairs(origin) do
        table.insert(tabla,e)
    end
    return tabla
end

function copyValue(origin)
    if origin == nil then
        return {}
    end
    value = origin[2]
    return value
end

-- rules = positions where the player put his piece (from 1 to 9)

-- La función getRules se encarga de calcular y devolver una lista con las reglas disponibles para el estado s. Para ello,
-- extraemos el contenido de s mediante la función getContent, declaramos la lista que almacenará cada una de las posiciones
-- vacías en el tablero y recorremos el tablero insertando en la lista cada una de las posiciones que tengan valor 0 para
-- finalmente devolver dicha lista
function handler.getRules (s)
    local c = getContent(s)
    local freePositions = {}
    for i,pos in pairs(c) do
        if pos==0 then
            table.insert(freePositions,i)
        end
    end
    return freePositions
  end


-- state: {content,player}
-- In this case, the content is the positions of chips ({p0, p1, p2, p3, p4, p5, p6, p7, p8}), and the player will be 1 or 2.
-- Where p(i) can be 0 (no piece), 1 (piece from player 1) or 2 (piece from player 2)

-- La función getContent se encarga de devolver el contenido del juego del estado s
function getContent (s)
    return s[1]
end
  
-- La función getPlayerJustMoved se encarga de devolver al jugador que acaba de mover
-- para alcanzar el estado s
function handler.getPlayerJustMoved (s)
    return s[2]
end
  
-- La función createState se encarga de crear y devolver un estado del juego a partir del contenido c y el jugador p
function createState (c,p)
    return {c,p}
end
  
-- La función apply se encarga de aplicar la regla r al estado s y crear y devolver el estado resultante
function handler.apply (r,s)
    local c = getContent(s)
    local p = handler.getPlayerJustMoved(s)
    -- Teniendo almacenado el contenido del estado s en c (mediante la función copyTable, que evitará problemas relacionados
    -- con la mutabilidad de la tabla) y el jugador que acaba de mover en p, insertamos el número correspondiente al jugador p
    -- en la casilla correspondiente de c y devolvemos
    -- c como siguiente estado y (3 - p) como el siguiente jugador
    c[r]=(3-p)
    return createState(c,(3 - p))
end
  
-- La función getResult se encarga de evaluar el valor de un estado terminal s para un jugador. Devolverá
-- 1 si el jugador p es el ganador y 0 en caso contrario
function handler.getResult (s,p)
    local pl = handler.getPlayerJustMoved(s)
    local c = getContent(s)
    -- Después de almacenar al jugador que acaba de mover en pl y el contenido del estado s en c,
    -- declaramos la tabla Lines, que representará cada una de las líneas de tablero posibles con las que se
    -- puede ganar el juego
    local Lines = {{1,2,3},{4,5,6},{7,8,9},{1,4,7},{2,5,8},{3,6,9},{1,5,9},{3,5,7}}
    -- A continuación, recorremos cada una de estas líneas comprobando si se cumple en el contenido del estado s (c).
    -- Si es así, pasamos a comprobar si el jugador ganador es pl y devolvemos 1 en caso afirmativo o 0 en caso contrario
    for i,pos in pairs(Lines) do
        if c[pos[1]]==c[pos[2]] and c[pos[2]]==c[pos[3]] and c[pos[1]]~=0 then
            if pl == p then 
                return 1
            else 
                return 0 
            end
        end
    end
    -- En caso de que ninguna de las líneas ganadores estén presentes en el tablero, comprobamos que efectivamente no quedan
    -- reglas disponibles y devolvemos 0.5 como valor de empate del juego. En caso de no tratarse de un estado final del juego
    -- , devolvemos el valor booleano false
    if #handler.getRules(s)==0 then
        return 0.5
    else
        return false
    end
end

-- La función AddChild se encarga de añadir un hijo al nodo nodep aplicándole la regla r. Comenzará creando al nodo hijo
-- (creando una instancia del resultado de aplicar la función apply) y actualizando sus atributos de padre y regla
function handler.AddChild (nodep,r)
    local child = Node(handler.apply(r,nodep.state))
    child.parent = nodep
    child.rule = r
    -- Recorremos la lista de reglas disponibles de nodep y eliminamos aquella que acabamos de aplicar
    for i,rule in pairs(nodep.untriedRules) do
        if rule == r then
            table.remove(nodep.untriedRules,i)
        end
    end
    -- Finalmente añadimos el nodo hijo a la lista de hijos de nodep y lo devolvemos
    table.insert(nodep.childs, child)
    return child
end

-- La función generatesChilds se encarga de generar todos los nodos sucesores (hijos)
-- de node, añadirlos a su lista de hijos y devolver una lista con ellos
function handler.generatesChilds(node)
    local estadoNodo = copyTable(node.state[1])
    for i,rule in pairs(handler.getRules(node.state)) do
        handler.AddChild(node,rule)
        node.state[1] = copyTable(estadoNodo)
    end
end

-- Funciones auxiliares de representación --

function printStateTictactoe(posiciones)
    io.write("INT::")
    for i,pos in ipairs(posiciones) do
      io.write(", "..pos)
    end
    io.write("\n")
  end

return handler