-- Implementación del Problema del Puente y la linterna

-- Como en cada uno de los problemas, importamos el archivo node para 
-- implementar el uso de los nodos
local Node = require ('LuaSearx.utils.node')

-- Definimos las funciones initialize, toString y isEqualTo, que indicarán la forma, la representación y la función
-- de comparación de cada uno de los nodos del problema. Los atributos p1 y p2 no serán utilizados para el desarrollo
-- del problema, sino que simplemente serán un apoyo para la futura representación
function Node:initialize(p1, p2, t, n, nS, l) self.p1, self.p2, self.t, self.n, self.nS, self.l = p1, p2, t, n, nS, l end
function Node:toString() return ('Node: p1 = %d, p2 = %d, t = %d'):format(self.p1, self.p2, self.t) end
function Node:isEqualTo(n) return self.p1 == n.p1 and self.p2 == n.p2 and self.t == n.t end

--[[

Estado -> ( p1 , p2 , t , n, nS, l)

t -> cantidad de segundos restantes para cruzar todos el puente
n -> lista de personas que se encuentran en el lado 1 (antes del puente)
nS -> lista de personas que se encuentran en el lado 2 (después del puente)
l -> la linterna está en el comienzo vale 1, si está al final vale 2

p1, p2 -> variables informativas que nos ayudarán en la representación

]]--

-- Declaramos las tablas nodes, que almacenará todos los nodos del problema, y handler, que será la tabla devuelta por este
-- archivo y permitirá acceder a los datos y funciones del problema
local nodes = {}
local handler = {}

-- La función init se encarga de inicializar el espacio de estados. A medida que se contruya el árbol se irán añadiendo
-- en nodes todos los nodos visitados
function handler.init()
  nodes = {}
end

-- La función getAllNodes devuelve la tabla nodes
function handler.getAllNodes() return nodes end

-- La función getNode se encarga de devolver al nodo (de la tabla nodes) correspondiente dado su estado.
-- Recorrerá los nodos de nodes y, si alguno coincide con los parámetros recibidos, devolverá el elemento.
-- De lo contrario creará el nodo, lo insertará en nodes y lo devolverá
function handler.getNode(p1, p2, t, n, nS, l)
    for i,e in ipairs(nodes) do
        if p1==e.p1 and p2==e.p2 and t==e.t and n==e.n and l==e.l then
            return nodes[i]
        end
    end
    table.insert(nodes,Node(p1, p2, t, n, nS, l))
    for i,e in ipairs(nodes) do
        if p1==e.p1 and p2==e.p2 and t==e.t and n==e.n and l==e.l then
            return nodes[i]
        end
    end
end

-- La función distance se encarga de devolver la distancia Manhattan entre los nodos a y b. Será utilizada en
-- los algoritmos de búsqueda con heurística. En esta ocasión la distancia entre nodos será la diferencia del tiempo
-- restante para cruzar el puente
function handler.distance(a, b)
    return (a.t - b.t)
end

-- La función checkTime se encarga de comprobar si un estado cumple con los requisitos del problema
function checkTime(state)
  if state[3] >= 0 then
    return true
  end
  return false
end 

--Pasamos ahora a definir las funciones que representarán a cada uno de los operadores del problema

--Dos personas p1 y p2 cruzan el puente y p1 vuelve con la linterna
function bridgeAndBackP1(nodo,p1,p2)
    local indexP1 = nil
    local indexP2 = nil
    for i,e in ipairs(nodo.n) do
        if e==p1 then indexP1=i end
        if e==p2 then indexP2=i end
    end
    table.remove(nodo.n, indexP2)
    table.insert(nodo.nS, p2)
      return {01, 0, nodo.t-math.max(p1,p2)-p1, nodo.n, nodo.nS, 1}
end

--Dos personas p1 y p2 cruzan el puente y p2 vuelve con la linterna
function bridgeAndBackP2(nodo,p1,p2)
    local indexP1 = nil
    local indexP2 = nil
    for i,e in ipairs(nodo.n) do
        if e==p1 then indexP1=i end
        if e==p2 then indexP2=i end
    end
    table.remove(nodo.n, indexP1)
    table.insert(nodo.nS, p1)
      return {02, 0, nodo.t-math.max(p1,p2)-p2, nodo.n, nodo.nS, 1}
end

--Dos personas p1 y p2 cruzan el puente y ninguna vuelve
function bridgeAndNoBack(nodo,p1,p2)
    local indexP1 = nil
    local indexP2 = nil
    for i,e in ipairs(nodo.n) do
        if e==p1 then indexP1=i end
    end
    table.remove(nodo.n, indexP1)
    table.insert(nodo.nS, p1)
    for i,e in ipairs(nodo.n) do
        if e==p2 then indexP2=i end
    end
    table.remove(nodo.n, indexP2)
    table.insert(nodo.nS, p2)
      return {03, 0, nodo.t-math.max(p1,p2), nodo.n, nodo.nS, 2}
end

--Una persona p1 cruza el puente y no vuelve
function bridgeOne(nodo,p1)
    local indexP1 = nil
    for i,e in ipairs(nodo.n) do
        if e==p1 then indexP1=i end
    end
    table.remove(nodo.n, indexP1)
    table.insert(nodo.nS, p1)
      return {04, 0, nodo.t-p1, nodo.n, nodo.nS, 2}
end

--Una persona p1 regresa con la linterna
function backOne(nodo,p1)
    local indexP1 = nil
    for i,e in ipairs(nodo.nS) do
        if e==p1 then indexP1=i end
    end
    table.remove(nodo.nS, indexP1)
    table.insert(nodo.n, p1)
      return {05, 0, nodo.t-p1, nodo.n, nodo.nS, 1}
end

--Dos personas regresan con la linterna
function backTwo(nodo,p1,p2)
    local indexP1 = nil
    local indexP2 = nil
    for i,e in ipairs(nodo.nS) do
        if e==p1 then indexP1=i end
    end
    table.remove(nodo.nS, indexP1)
    table.insert(nodo.n, p1)
    for i,e in ipairs(nodo.nS) do
        if e==p2 then indexP2=i end
    end
    table.remove(nodo.nS, indexP2)
    table.insert(nodo.n, p2)
    return {06, 0, nodo.t-math.max(p1,p2), nodo.n, nodo.nS, 1}
end

-- La función areInNs se encarga de comprobar si e1 y e2 se encuentran actualmente en la lista nodo.nS
function areInNs(nodo,e1,e2)
    isE1=false
    isE2=false
    for i,element in ipairs(nodo.nS) do
        if element==e1 then isE1=true end
        if element==e2 then isE2=true end
    end
    if isE1 and isE2 then return true else return false end
end

-- La función isInNs se encarga de comprobar si e1 se encuentra actualmente en la lista nodo.nS
function isInnS(nodo,e1)
    for i,element in ipairs(nodo.nS) do
        if element==e1 then return true else return false end
    end
end

-- La función getNeighbors se encarga de calcular cada uno de los nodos sucesores (hijos) del
-- nodo n y devuelve una lista con todos ellos
function handler.getNeighbors(n)
  local neighbors = {}
  local p1, p2, t, n, nS, l = n.p1, n.p2, n.t, n.n, n.nS, n.l
  nodo=handler.getNode(p1, p2, t, n, nS, l)
        -- Teniendo almacenado el nodo actual en la variable nodo, pasamos a recorrer los elementos de nodo.n
        -- comprobando si es posible realizar la acción del operador bridgeOne y, en ese caso, realizándola y añadiendo el
        -- resultado a la lista de vecinos. Este operador se ejecuta aquí debido a que es necesario que el elemento se encuentre
        -- en nodo.n y la linterna se encuentre en valor 1
    for index1,element1 in ipairs(nodo.n) do
        if checkTime(bridgeOne(copyNode(nodo),element1)) and nodo.l == 1 then
            local v = bridgeOne(copyNode(nodo),element1)
            table.insert(neighbors, handler.getNode(v[1],v[2],v[3],v[4],v[5],v[6]))
        end
        -- Además, recorremos cada posible par de elementos de nodo.n, aplicamos los operadores bridgeAndBackP1
        -- bridgeAndBackP2 y bridgeAndNoBack (cuando sea posible) y añadimos esos nuevos vecinos a la lista.
        -- Estos operadores se ejecutan aquí debido a que es necesario que los elementos se encuentren
        -- en nodo.n y la linterna se encuentre en valor 1
        for index2, element2 in ipairs(nodo.n) do
            if element1 ~= element2 then
                if checkTime(bridgeAndBackP1(copyNode(nodo),element1,element2)) and nodo.l == 1 then
                    local v = bridgeAndBackP1(copyNode(nodo),element1,element2)
                    table.insert(neighbors, handler.getNode(v[1],v[2],v[3],v[4],v[5],v[6]))
                end
                if checkTime(bridgeAndBackP2(copyNode(nodo),element1,element2)) and nodo.l == 1  then
                    local v = bridgeAndBackP2(copyNode(nodo),element1,element2)
                    table.insert(neighbors, handler.getNode(v[1],v[2],v[3],v[4],v[5],v[6]))
                end
                if checkTime(bridgeAndNoBack(copyNode(nodo),element1,element2),true) and nodo.l == 1 then
                    local v = bridgeAndNoBack(copyNode(nodo),element1,element2)
                    table.insert(neighbors, handler.getNode(v[1],v[2],v[3],v[4],v[5],v[6]))
                end
            end
        end
    end
    -- Recorremos todos los elementos de nodo.nS comprobando si es posible realizar la acción del operador backOne y, si es
    -- así, la realizamos y añadimos el resultado a la lista de vecinos. Este operador se ejecuta aquí debido a que es necesario que el elemento se encuentre
        -- en nodo.nS y la linterna se encuentre en valor 2
    for index1,element1 in ipairs(nodo.nS) do
        if checkTime(backOne(copyNode(nodo),element1)) and nodo.l == 2 and isInnS(nodo,element1)  then
            local v = backOne(copyNode(nodo),element1)
            table.insert(neighbors, handler.getNode(v[1],v[2],v[3],v[4],v[5],v[6]))
        end
        -- Mientras tanto, recorremos cada posible par de elementos de nodo.nS, aplicando el operador backTwo 
        -- (cuando sea posible) y añadiendo esos nuevos vecinos a la lista. Estos operadores se ejecutan aquí debido a que es necesario que los elementos se encuentren
        -- en nodo.nS y la linterna se encuentre en valor 2
        for index1,element2 in ipairs(nodo.nS) do
            if checkTime(backTwo(copyNode(nodo),element1,element2)) and nodo.l == 2 
                                                                and areInNs(nodo,element1,element2) then
            local v = backTwo(copyNode(nodo),element1,element2)
            table.insert(neighbors, handler.getNode(v[1],v[2],v[3],v[4],v[5],v[6]))
            end
        end
    end
  return neighbors
end

-- La función copyNode se encarga de copiar todos los atributos de un nodo. De esta forma evita posibles problemas
-- derivados de la propiedad mutable de las tablas
function copyNode(node)
    local newNode = Node()
    local nList = {}
    for i,e in ipairs(node.n) do
      table.insert(nList,e)
    end
    local nSList = {}
    if #node.nS >= 0 then 
        for i,e in ipairs(node.nS) do
        table.insert(nSList,e)
        end
    end
    newNode.p1 = node.p1
    newNode.p2 = node.p2
    newNode.t = node.t
    newNode.n = nList
    newNode.nS = nSList
    newNode.l = node.l
  return newNode
  end

  -- Funciones auxiliares de representación

  function printeaEstadoPuentes(nodo)
    io.write("Nodo: "..nodo.p1.." - "..nodo.p2.." - Time: "..nodo.t.." - n: {")
    for i,e in ipairs(nodo.n) do
      io.write(e..", ")
    end
    io.write("} nS: {")
    for i,e in ipairs(nodo.nS) do
      io.write(e..", ")
    end
    io.write("} L: "..nodo.l.."\n")
end

return handler