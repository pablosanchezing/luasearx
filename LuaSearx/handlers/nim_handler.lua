-- Implementación del juego del Nim

-- Como en cada uno de los juegos, importamos el archivo node para 
-- implementar el uso de los nodos
local Node = require ('LuaSearx.utils.node')

-- Declaramos la tabla handler, que será la tabla devuelta por este
-- archivo y permitirá acceder a los datos y funciones del juego
local handler = {}

-- La función initialize se encarga de declarar los atributos de los que dispondrá cada uno
-- de los nodos del juego y cual será su valor inicial. Los atributos serán el nodo padre, la lista
-- de nodos hijo, el estado, las victorias y visitas acumuladas, la regla aplicada para alcanzar al nodo y 
-- la lista de reglas disponibles
function Node:initialize(state)
    self.parent = nil
    self.childs = {}
    self.state = state
    self.wins = 0
    self.visits = 0
    self.rule = 0
    self.untriedRules = handler.getRules(state)
    self.final = (state[1] == 0)
  end

-- La función createNode se encarga de llamar a la función initialize con el estado state
function handler.createNode(state) return Node(state) end

-- rules = number of chips to remove

-- La función getRules se encarga de calcular y devolver una lista con las reglas disponibles para el estado s. Haciendo
-- uso de la función range, que se encarga de devolver una lista de todos los números comprendidos en el rango {a, b-1}
function handler.getRules (s)
    return range(1,math.min(4,1+getContent(s)))
end

-- La función range se encarga de devolver una lista de todos los números comprendidos en el rango {a, b-1}
function range(a,b)
    if a < b then    
        local res = {}
        local current = a
        while b > current do
            table.insert(res,current)
            current = current +1
        end
        return res
    else 
        return {} 
    end
end

-- state: {content,player}
-- In this case, the content is the number of chips, and the player will be 1 or 2.

-- La función getContent se encarga de devolver el contenido del juego del estado s
function getContent (s)
    return s[1]
end
  
-- La función getPlayerJustMoved se encarga de devolver al jugador que acaba de mover
-- para alcanzar el estado s
function handler.getPlayerJustMoved (s)
    return s[2]
end
  
-- La función createState se encarga de crear y devolver un estado del juego a partir del contenido c y el jugador p
function createState (c,p)
    return {c,p}
end

-- La función apply se encarga de aplicar la regla r al estado s y crear y devolver el estado resultante
function handler.apply (r,s)
    local c = getContent(s)
    local p = handler.getPlayerJustMoved(s)
    -- Teniendo almacenado el contenido del estado s en c y el jugador que acaba de mover en p, devolvemos
    -- (c - r) como siguiente estado y (3 - p) como el siguiente jugador
    return createState((c - r),(3 - p))
end

-- La función getResult se encarga de evaluar el valor de un estado terminal s para un jugador. Devolverá
-- 1 si el jugador p es el ganador y 0 en caso contrario
function handler.getResult (s,p)
    local pl = handler.getPlayerJustMoved(s)
    if pl == p then 
      return 1
    else 
      return 0 
    end
end

-- La función AddChild se encarga de añadir un hijo al nodo nodep aplicándole la regla r. Comenzará creando al nodo hijo
-- (creando una instancia del resultado de aplicar la función apply) y actualizando sus atributos de padre y regla
function handler.AddChild (nodep,r)
    local child = Node(handler.apply(r,nodep.state))
    child.parent = nodep
    child.rule = r
    -- Recorremos la lista de reglas disponibles de nodep y eliminamos aquella que acabamos de aplicar
    for i,rule in pairs(nodep.untriedRules) do
        if rule == r then
            table.remove(nodep.untriedRules,i)
        end
    end
    -- Finalmente añadimos el nodo hijo a la lista de hijos de nodep y lo devolvemos
    table.insert(nodep.childs, child)
    return child
end

-- La función generatesChilds se encarga de generar todos los nodos sucesores (hijos)
-- de node, añadirlos a su lista de hijos y devolver una lista con ellos
function handler.generatesChilds(node)
    childs = {}
    for i,rule in pairs(handler.getRules(node.state)) do
        table.insert(childs,handler.AddChild(node,rule))
    end
    return childs
end

return handler