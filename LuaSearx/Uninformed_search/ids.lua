-- Implementación del algoritmo genérico de búsqueda en profundidad iterada
-- Para ser ejecutado deberá ser inicializado con un handler, que actuará de
-- interfaz entre el algortimo y el espacio de búsqueda

-- El handler para inicializar el algoritmo deberá definir las siguientes funciones:
-- handler.getNode(...)   ->  devuelve un nodo (instancia de node.lua)
-- handler.getNeighbors(n) -> devuelve una tabla que almacena todos los sucesores del nodo n

-- Importamos el archivo de class para implementar el uso de la clase del algoritmo
local class = require 'LuaSearx.utils.class'

-- Esta función construye el camino desde el nodo objetivo hasta el nodo raíz.
-- Nos devolverá una tabla con los nodos que conforman el camino.
local function backtrace(node)
  local path = {}
  repeat
    table.insert(path, 1, node)
    node = node.parent
  until not node
  return path
end

-- La función depthLimitedSearch realiza una búsqueda recursiva de profundidad limitada por el parámetro depth, desde
-- el nodo inicial (start) al nodo objetivo (goal) y utilizando las funciones de la clase del algoritmo 
-- (finder en este caso)
local function depthLimitedSearch(finder, start, goal, depth)
  -- Comprobamos si el nodo inicial es el nodo objetivo (en ese caso construimos el camino y lo devolvemos)
  -- o si se ha alcanzado la profundidad determinada por depth (en ese caso no se ha encontrado aún la solución)

  --Es posible que sea necesario modificar esta línea para lograr una mejor adaptación de su problema
  if start == goal or isGoal(start,goal) then return backtrace(start) end

  if depth == 0 then return end
  -- Marcamos el nodo inicial como visitado y generamos cada uno de los nodos hijo 
  -- (que llamamos vecinos) del nodo actual.
  start.visited = true
  local neighbors = finder.handler.getNeighbors(start)
  -- A continuación recorremos la lista de vecinos actualizando la información de cada uno de ellos.
  -- Si no están marcados como visitados definimos a su nodo padre como el nodo inicial
  for _, neighbor in ipairs(neighbors) do
    if not neighbor.visited then
      neighbor.parent = start
      -- Volvemos a llamar a la función depthLimitedSearch pasándole como argumento el nodo vecino (start) y el
      -- nodo objetivo (goal) pero esta vez disminuyendo el parámetro depth en 1 unidad.
      -- Si en esa ejecución se encuentra la solución, se devuelve el resultado.
      local foundGoal = depthLimitedSearch(finder, neighbor, goal, depth - 1)
      if foundGoal then return foundGoal end
    end
  end
end

-- Definimos la clase del algoritmo de búsqueda en profundidad iterada
local IDS = class()
-- Esta función inicializa el algoritmo con el handler seleccionado y define la profundidad máxima que
-- alcanzará la búsqueda determinada por el parámetro maxDepth (en caso de recibirlo se establecerá por defecto en 15)
function IDS:initialize(handler, maxDepth)
  self.handler = handler
  self.maxDepth = maxDepth or 15 -- Profundidad predeterminada
end

-- La función isGoal comprueba si node es un estado solución cuando este se trata de una tabla
-- compuesta de tablas
function isGoal(node,goal)
  if type(node.n) == type({}) and #node.n == 0 then return true end
  if type(node.subconjunto) == type({}) and node.datos[1] == goal and node.datos[2] == 0 then return true end
    return false
end

-- Esta función limpia la información contenida en cada nodo. La utilizaremos para limpiar los datos 
-- de nodos entre cada iteración de búsqueda
function IDS:resetForNextSearch()
  local nodes = self.handler.getAllNodes()
  for _, node in ipairs(nodes) do
    node.visited, node.parent = nil, nil
  end
end

-- Es la función principal del algoritmo y a la que llamaremos para encontrar la
-- solución del problema y el camino recorrido. Sus argumentos son el nodo inicial (start)
-- y el nodo objetivo que consideramos solución (goal).
function IDS:findPath(start, goal)
  -- Con este bucle recorremos todos los valores de profundidad entre 1 y maxDepth
  for depth = 1, self.maxDepth do
    -- Limpiamos los datos de los nodos de búsquedas o iteraciones anteriores
    self:resetForNextSearch()
    -- Llamamos a la función depthLimitedSearch para que realice una búsqueda con la profundidad actual.
    -- Si en esa búsqueda se encuentra la solución, se devuelve.
    local p = depthLimitedSearch(self, start, goal, depth)
    if p then return p end
  end
end

return IDS