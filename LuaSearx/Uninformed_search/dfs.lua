-- Implementación del algoritmo genérico de búsqueda en profundidad
-- Para ser ejecutado deberá ser inicializado con un handler, que actuará de
-- interfaz entre el algortimo y el espacio de búsqueda

-- Esta implementación hace uso de una cola FIFO para los nodos

-- El handler para inicializar el algoritmo deberá definir las siguientes funciones:
-- handler.getNode(...)   ->  devuelve un nodo (instancia de node.lua)
-- handler.getNeighbors(n) -> devuelve una tabla que almacena todos los sucesores del nodo n

-- Importamos los archivos de class y lifo para implementar el uso de la clase del algoritmo
-- y de la cola de nodos, respectivamente.
local class = require 'LuaSearx.utils.class'
local lifo  = require 'LuaSearx.utils.lifo'

-- Esta función limpia la información de cada nodo, la cola de nodos y la lista de nodos visitados.
-- La utilizaremos para limpiar los datos de nodos antes de comenzar una nueva búsqueda.
local function resetForNextSearch(dfs)
  for node in pairs(dfs.visited) do
    node.parent, node.visited = nil, nil
  end
  dfs.stack:clear()
  dfs.visited = {}
end

-- Esta función construye el camino desde el nodo objetivo hasta el nodo raíz.
-- Nos devolverá una tabla con los nodos que conforman el camino.
local function backtrace(node)
  local path = {}
  repeat
    table.insert(path, 1, node)
    node = node.parent
  until not node
  return path
end

-- La función isGoal comprueba si node es un estado solución cuando este se trata de una tabla
-- compuesta de tablas
function isGoal(node,goal)
  if type(node.n) == type({}) and #node.n == 0 then return true end
  if type(node.subconjunto) == type({}) and node.datos[1] == goal and node.datos[2] == 0 then return true end
    return false
end

-- Definimos la clase del algoritmo de búsqueda en profundidad
local DFS = class()
-- Esta función inicializa el algoritmo con el handler seleccionado, la cola lifo 
-- que llamaremos stack, ya que se asemeja al funcionamiento de una pila
-- y la lista de nodos visitados vacía
function DFS:initialize(handler)
  self.handler = handler
  self.stack = lifo()
  self.visited = {}
end

-- Es la función principal del algoritmo y a la que llamaremos para encontrar la
-- solución del problema y el camino recorrido. Sus argumentos son el nodo inicial (start)
-- y el nodo objetivo que consideramos solución (goal).
function DFS:findPath(start, goal)
  resetForNextSearch(self)
  
  start.visited = true
  self.stack:push(start)
  self.visited[start] = true
  while not self.stack:isEmpty() do
    local node = self.stack:pop()

    --Es posible que sea necesario modificar esta línea para lograr una mejor adaptación de su problema
    if node == goal or isGoal(node,goal) then return backtrace(node) end

    local neighbors = self.handler.getNeighbors(node)
    for _, neighbor in ipairs(neighbors) do
      if not neighbor.visited then
        neighbor.visited = true
        neighbor.parent = node
        self.stack:push(neighbor)
        self.visited[neighbor] = true
      end
    end
  end
end

return DFS