-- Implementación del algoritmo genérico de búsqueda en anchura

-- Para ser ejecutado deberá ser inicializado con un handler, que actuará de
-- interfaz entre el algortimo y el espacio de búsqueda

-- Esta implementación hace uso de una cola FIFO para los nodos

-- El handler para inicializar el algoritmo deberá definir las siguientes funciones:
-- handler.getNode(...)   ->  devuelve un nodo (instancia de node.lua)
-- handler.getNeighbors(n) -> devuelve una tabla que almacena todos los sucesores del nodo n

-- Importamos los archivos de class y fifo para implementar el uso de la clase del algoritmo
-- y de la cola de nodos, respectivamente.
local class = require 'LuaSearx.utils.class'
local fifo  = require 'LuaSearx.utils.fifo'

-- Esta función limpia la información de cada nodo, la cola de nodos y la lista de nodos visitados.
-- La utilizaremos para limpiar los datos de nodos antes de comenzar una nueva búsqueda.
local function resetForNextSearch(bfs)
  for node in pairs(bfs.visited) do
    node.parent, node.visited = nil, nil
  end
  bfs.queue:clear()
  bfs.visited = {}
end

-- Esta función construye el camino desde el nodo objetivo hasta el nodo raíz.
-- Nos devolverá una tabla con los nodos que conforman el camino.
local function backtrace(node)
  local path = {}
  repeat
    table.insert(path, 1, node)
    node = node.parent
  until not node
  return path
end

-- La función isGoal comprueba si node es un estado solución cuando este se trata de una tabla
-- compuesta de tablas
function isGoal(node,goal)
  if type(node.n) == type({}) and #node.n == 0 then return true end
  if type(node.subconjunto) == type({}) and node.datos[1] == goal and node.datos[2] == 0 then return true end
    return false
end

-- Definimos la clase del algoritmo de búsqueda en anchura
local BFS = class()
-- Esta función inicializa el algoritmo con el handler seleccionado, la cola fifo y la lista de nodos visitados vacía
function BFS:initialize(handler)
  self.handler = handler
  self.queue = fifo()
  self.visited = {}
end

-- Es la función principal del algoritmo y a la que llamaremos para encontrar la
-- solución del problema y el camino recorrido. Sus argumentos son el nodo inicial (start)
-- y el nodo objetivo que consideramos solución (goal).
function BFS:findPath(start, goal)
  -- Limpiamos los datos de los nodos de búsquedas anteriores
  resetForNextSearch(self)
  -- Marcamos el nodo inicial como visitado, lo añadimos a la cola y lo marcamos como
  -- visitado en la lista de nodos visitados
  start.visited = true
  self.queue:push(start)
  self.visited[start] = true
  -- Con este bucle recorremos todo los estados necesarios hasta dar con la solución.
  -- Extraemos un nodo de la cola, comprobamos si es un nodo solución (en ese caso construimos
  -- el camino y lo devolvemos) y generamos cada uno de los nodos hijo (que llamamos vecinos) del nodo actual.
  while not self.queue:isEmpty() do 
    --Esto se repetirá hasta que el nodo actual sea igual a la solución o no queden nodos en la cola
    local node = self.queue:pop()

    --Es posible que sea necesario modificar esta línea para lograr una mejor adaptación de su problema
    if node == goal or isGoal(node,goal) then return backtrace(node) end
    
    local neighbors = self.handler.getNeighbors(node)
    -- A continuación recorremos la lista de vecinos actualizando la información de cada uno de ellos,
  -- si no están marcados como visitados:
  -- ·Los marcamos como visitados y definimos quién es su nodo padre (nodo actual)
  -- ·Lo introducimos en la cola
  -- ·Lo marcamos como visitado en la lista de nodos visitados.
    for _, neighbor in ipairs(neighbors) do
      if not neighbor.visited then
        neighbor.visited = true
        neighbor.parent = node
        self.queue:push(neighbor)
        self.visited[neighbor] = true
      end
    end
  end
end

return BFS