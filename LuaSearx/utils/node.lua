local PATH = (...):gsub('%.node$','')
local class = require (PATH .. '.class')

local Node = class ()
Node.__eq = function(a, b) return a:isEqualTo(b) end
Node.__tostring = function(n) return n:toString() end

return Node