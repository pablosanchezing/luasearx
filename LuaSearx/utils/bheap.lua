-- Implementación de la estructura de datos Binary Heap (Montículo binario)

local PATH = (...):gsub('%.bheap$','')
local class = require (PATH .. '.class')

-- La función findIndex busca y devuelve el índice de un elemento si este se
-- encuentra en un array
local function findIndex(array, item)
  for k,v in ipairs(array) do
    if v == item then return k end
  end
end

-- La función sift_up se encarga de restaurar la propiedad del montículo. Para ello, va remontando hasta la raíz del
-- montículo intercambiando el elemento de la posición desornedada por el elemento de su padre
local function sift_up(bheap, index)
  -- Comprueba si el índice (index) es el primer elemento del montículo (en esos casos acaba y no devuelve nada). 
  -- De lo contrario, si el índice es multiplo de 2, divide el valor de index entre 2 y lo almacena en pIndex (en caso de no ser múltiplo)
  -- lo hace con index-1). Con esto, estamos almacenando en pIndex al índice del padre de index
  if index <= 1 then return end
  local pIndex
  if index%2 == 0 then
    pIndex = index/2
  else pIndex = (index-1)/2
  end
  -- A continuación, comprueba si el tamaño del elemento padre (pIndex) es menor que el del elemento de índice índex
  -- (a través de la función _sort). Si NO es así, intercambia los elementos de ambos índices
  if not bheap._sort(bheap._heap[pIndex], bheap._heap[index]) then
    bheap._heap[pIndex], bheap._heap[index] = bheap._heap[index], bheap._heap[pIndex]
    -- Después de esto, llama a la función sift_up (a si misma) de nuevo. De esta forma logramos una función recursiva
    -- que filtra los elementos del montículo hasta que este haya recuperado su propiedad de Binary Heap
    sift_up(bheap, pIndex)
  end
end

-- La función sift_down se encarga de reorganizar el montículo para que vuelva a satisfacer su propiedad. A diferencia de
-- la función sift_up, que se remonta hasta la raíz del montículo, esta comienza en la raíz y va descendiendo por el montículo.
-- La función sift_up será utilizada cada vez que se agrega un elemento al montículo (push) y sift_down cuando uno de los elementos sea
-- extraído (pop)
local function sift_down(bheap,index)
  -- Almacenamos los índices que corresponden a los hijos de index. A continuación, comprobamos que ambos 
  -- índices son menores que el tamaño del montículo y almacenamos el índice minIndex, que corresponderá al hijo mínimo
  -- del elemendo de index
  local lfIndex,rtIndex,minIndex
  lfIndex = 2*index
  rtIndex = lfIndex + 1
  if rtIndex > bheap.size then
    if lfIndex > bheap.size then return
    else minIndex = lfIndex  end
  else
    -- Ahora, si el tamaño de un elemento hijo es menor que el del otro hijo, actualizamos el índice del hijo mínimo
    -- por el nuevo
    if bheap._sort(bheap._heap[lfIndex],bheap._heap[rtIndex]) then
      minIndex = lfIndex
    else
      minIndex = rtIndex
    end
  end
  -- Después comprobamos que el tamaño del elemento de index es menor al de minIndex. Si NO es así, intercambia los
  -- elementos de ambos índices
  if not bheap._sort(bheap._heap[index],bheap._heap[minIndex]) then
    bheap._heap[index],bheap._heap[minIndex] = bheap._heap[minIndex],bheap._heap[index]
    -- Finalmente llamamos a la función sift_down (a si misma) de nuevo. Haciendo esto conseguimos una función recursiva
    -- que filtra los elementos del montículo hasta que este recupere su propiedad
    sift_down(bheap,minIndex)
  end
end

-- Definimos la clase de la estructura de datos Binary Heaps (minHeaps por defecto)
local bheap = class()
-- Esta función inicializa la estructura de datos, definiendo su tamaño inicial (0), la función que define el orden y el 
-- montículo inicial vacío
function bheap:initialize()
  self.size = 0
  self._sort = function(a,b) return #a < #b end
  self._heap = {}
end

-- La función clear se encarga de vaciar el montículo y devolver su tamaño a 0
function bheap:clear()
  self._heap = {}
  self.size = 0
end

-- La función isEmpty se encarga de comprobar si el tamaño del montículo es 0
function bheap:isEmpty()
  return (self.size==0)
end

-- La función push se encarga de introducir un elemento al final del montículo.
-- A continuación llama a la función sift_up para que reorganice los elementos del montículo a partir del nuevo elemento
function bheap:push(item)
  self.size = self.size + 1
  self._heap[self.size] = item
  sift_up(self, self.size)
end

-- La función pop se encarga de extraer un elemento del montículo y devolverlo
function bheap:pop()
  local root
  -- Comprobamos que el tamaño es mayor que 0 y, si es así, extraemos el primer elemento del montículo 
  -- (el menor por defecto), colocamos el último elemento (la posición que debe quedar vacía) en la raíz, 
  -- disminuimos el tamaño del montículo en 1 unidad y llamamos a la función sift_down para que reorganice los 
  -- elementos del montículo hasta recuperar su propiedad
  if self.size > 0 then
    root = self._heap[1]
    self._heap[1] = self._heap[self.size]
    self._heap[self.size] = nil
    self.size = self.size-1
    if self.size > 1 then
      sift_down(self, 1)
    end
  end
  return root
end

-- La función sort se encarga de ordenar un elemento específico dentro del montículo
function bheap:sort(item)
  -- Comprueba si el tamaño del montículo es mayor que 1 (si no es así se acaba). A continuación, llama a la
  -- función findIndex para obtener el índice del elemento y a la función sift_up para que reorganice los 
  -- elementos del montículo a partir del elemento i
  if self.size <= 1 then return end
  local i = findIndex(self._heap, item)
  if i then sift_up(self, i) end
end

return bheap