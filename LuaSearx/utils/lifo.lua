-- Implementación de la cola (o pila) LIFO (Last In First Out)

-- Importamos el archivo de class, definimos la clase de la cola LIFO e inicializamos la pila como una tabla vacía
local PATH = (...):gsub('%.lifo$','')
local class = require (PATH .. '.class')

local lifo = class()
lifo._stack = {}

-- La función push se encarga de introducir un valor (value) al comienzo de la cola
function lifo:push(value) table.insert(self._stack, 1, value) end

-- La función pop se encarga de eliminar y devolver el valor que se encuentra al comienzo de la cola
function lifo:pop()
  local head = self._stack[1]
  table.remove(self._stack, 1)
  return head
end

-- Las funciones clear y isEmpty son definidas de la misma forma que en la cola FIFO vista anteriormente.

-- Clears the heap
function lifo:clear() self._stack = {} end

-- Checks if the heap is empty
function lifo:isEmpty() return #self._stack == 0 end

return lifo