-- Implementación de la cola FIFO (First In First Out)

-- Importamos el archivo de class, definimos la clase de la cola FIFO e inicializamos la cola como una tabla vacía
local PATH = (...):gsub('%.fifo$','')
local class = require (PATH .. '.class')

local fifo = class()
fifo._queue = {}

-- Introduce un valor al final de la cola
function fifo:push(value) table.insert(self._queue, value) end

-- Elimina y devuelve el primer valor de la cola
function fifo:pop()
  local head = self._queue[1]
  table.remove(self._queue, 1)
  return head
end

-- Limpia la cola
function fifo:clear(f) self._queue = {} end

-- Comprueba si la cola está vacía
function fifo:isEmpty() return #self._queue == 0 end

return fifo