-- Implementación del juego Nim con el algoritmo Minimax

local Minimax = require 'LuaSearx.Adversarial_search.minimax'

local nim_handler = require 'LuaSearx.handlers.nim_handler'
local minimax = Minimax(handler)
  
-- Declaramos la variable que almacena el número de fichas iniciales
local num_chips = 21
  
-- La función removeChips se encarga de eliminar n fichas del montón (turno del jugador humano).
-- Después, comprueba si el juego ha finalizado y, si no es así, ejecuta el turno de la IA, que
-- utilizará Minimax para elegir su movimiento
function removeChips (n)
    if n <= num_chips then
        num_chips = num_chips - n
    end
    if num_chips == 0 then
        print("Has ganado.")
    else
        local m = minimax:findChoice({num_chips,1},true,nim_handler)[1]
        print("Yo elimino " .. m .. " fichas")
        num_chips = num_chips - m
        if num_chips == 0 then
            print("He ganado.")
        end
    end
end

-- Mientras la partida no haya finalizado el programa preguntará al jugador humano
-- cuántas fichas desea retirar en su turno y para retirarlas llamará a la función removeChips
while num_chips ~= 0 do
    io.write("Quedan "..num_chips.." fichas\n")
    io.write("Cuantas quieres quitar?\n")
    local num = io.read("n")
    if num < 4 and num > 0 and num <= num_chips then
        io.write("Tu eliminas "..num.." fichas\n")
        removeChips(num)
    else 
        io.write("ERROR\n")
    end
end