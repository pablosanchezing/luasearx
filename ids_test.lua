-- Archivo de prueba del algoritmo de búsqueda no informada IDS

local IDS = require 'LuaSearx.Uninformed_search.ids'

----[[ INICIO PROBLEMA PUENTE Y LINTERNA 

--(Para este algoritmo, el tiempo de ejecución de este problema es muy reducido)

-- Función de representación
function printEstadoPuentes(nodo)
    io.write("Nodo: Operador: "..nodo.p1.." - Tiempo: "..nodo.t.." - n: {")
    for i,e in ipairs(nodo.n) do
      io.write(e)
      if  i ~= #nodo.n then
        io.write(", ") end
    end
    io.write("} nS: {")
    for i,e in ipairs(nodo.nS) do
      io.write(e)
      if  i ~= #nodo.nS then
          io.write(", ") end
    end
    io.write("} L: "..nodo.l.."\n")
end

print("Problema del puente y la linterna")
local handler = require 'LuaSearx.handlers.puente_handler'
local ids = IDS(handler)
handler.init()

local start, goal = handler.getNode(0,0,900,{60,120,300,480},{},1), handler.getNode(7,_,0,_,_,_)

local t1 = os.clock()
camino = ids:findPath(start, goal)
local t2 = os.clock()

print("Camino recorrido:")
for i,x in ipairs(camino) do
  printEstadoPuentes(x)
end

print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos")

--FIN ]]--

--[[ INICIO PROBLEMA MISIONEROS
  
print("Problema de los misioneros y los canivales")
local handler = require 'LuaSearx.handlers.misioneros_handler'
local ids = IDS(handler)
handler.init()

local start, goal = handler.getNode(3,3,1), handler.getNode(0,0,0)

local t1 = os.clock()
camino = ids:findPath(start, goal)
local t2 = os.clock()

for i,x in ipairs(camino) do
  print(x)
end

print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos")

--FIN ]]--

--[[INICIO PROBLEMA JARRAS

print("Problema de las dos jarras")
local handler = require 'LuaSearx.handlers.jarras_handler'
local ids = IDS(handler)
local map = {{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}}
handler.init(map)

local start, goal = handler.getNode(4,3), handler.getNode(2,0)

local t1 = os.clock()
camino = ids:findPath(start, goal)
local t2 = os.clock()

for i,x in ipairs(camino) do
  print(x)
end

print("Tiempo de ejecucion: " .. t2 - t1 .. " segundos") 

--FINAL]]--